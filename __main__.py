# Strings to be converted into a list. The strings must be separated by linebreak.
strings = """
REPLACE HERE
"""

string_list: list = strings.split(sep='\n')
filtered = filter(lambda string: string != '', string_list)
string_list = list(filtered)
print(string_list)
f = open('string_list.txt', mode='w')
f.write(str(string_list))
f.close()